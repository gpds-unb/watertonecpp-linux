#ifndef MASKMANAGER_H
#define MASKMANAGER_H

#include <vector>
#include <map>

#include "shared_types.h"
#include "../src/cppitertools/product.hpp"

using namespace std;

class MaskManager
{
    public:
        MaskManager();
        virtual ~MaskManager();
        vector<mask_t> getMasks(triplet_t);
        vector<mask_t> getMasks(void);
        vector<triplet_t> getTriplets(void);
        triplet_t getTripletFromMask(mask_t);
    protected:
    private:
        void create_masks(void);
        void create_triplets(void);
        void create_maps(void);

        vector<triplet_t> triplets;
        vector<mask_t> masks;
        map<triplet_t, vector<mask_t>> mapedMaks;
        map<mask_t, triplet_t> mapedTriplets;
};

#endif // MASKMANAGER_H
