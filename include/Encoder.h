#ifndef ENCODER_H
#define ENCODER_H

#include <opencv2/opencv.hpp>

#include "constants.h"
#include "shared_types.h"
#include "MaskManager.h"
#include "Halftoning.h"
#include "AbstractCodec.h"

class Encoder : AbstractCodec
{
    public:
        Encoder(cv::Mat, cv::Mat);
        virtual ~Encoder();
        cv::Mat getMasked(void);
        cv::Mat getHostHalftone(void);
    protected:
        cv::Mat hR, hG, hB;
        cv::Mat R, G, B;
        cv::Mat hostHalftone;
        cv::Mat codedHalftone;
        vector<triplet_t> triplets;
        vector<mask_t> embeddedMasks;
    private:
        void setRGB(cv::Mat);
        void computeHostHalftone(cv::Mat);
        void computeMarkHalftones(void);
        void computeTriplets(void);
        void embedMark(void);
        vector<float> computeDistances(mask_t, vector<mask_t>);
        float getSimmilarityIndex(mask_t, mask_t);
        uint getMinimumIndex(vector<float>);
        mask_t getBestMatch(triplet_t, mask_t);
        void convertEmbeddedMasksToHalftone(void);
};

#endif // ENCODER_H
