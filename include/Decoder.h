#ifndef DECODER_H
#define DECODER_H

#include <opencv2/opencv.hpp>

#include "constants.h"
#include "shared_types.h"
#include "MaskManager.h"
#include "Halftoning.h"
#include "AbstractCodec.h"

class Decoder : AbstractCodec
{
    public:
        Decoder(cv::Mat);
        virtual ~Decoder();
        cv::Mat getDecoded(void);
    protected:
        cv::Mat hR, hG, hB;
        cv::Mat R, G, B;
        cv::Mat decoded;
        cv::Mat coded;

        void computeTripletsFromMasks(void);
        void recoverHalftoneChannels(void);
        void recoverColorChannels(void);
    private:
        vector<triplet_t> triplets;
};

#endif // DECODER_H
