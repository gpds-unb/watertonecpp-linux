#------------------------------------------------------------------------------#
# This makefile was generated by 'cbp2make' tool rev.147                       #
#------------------------------------------------------------------------------#


WORKDIR = `pwd`

CC = gcc
CXX = g++
AR = ar
LD = g++
WINDRES = windres

INC = 
CFLAGS = -std=c++11 -Wall -fexceptions -lopencv_calib3d -lopencv_contrib -lopencv_core -lopencv_features2d -lopencv_flann -lopencv_gpu -lopencv_highgui -lopencv_imgproc -lopencv_legacy -lopencv_ml -lopencv_objdetect -lopencv_ocl -lopencv_photo -lopencv_stitching -lopencv_superres -lopencv_ts -lopencv_video -lopencv_videostab
RESINC = 
LIBDIR = 
LIB = 
LDFLAGS = -lopencv_calib3d -lopencv_contrib -lopencv_core -lopencv_features2d -lopencv_flann -lopencv_gpu -lopencv_highgui -lopencv_imgproc -lopencv_legacy -lopencv_ml -lopencv_objdetect -lopencv_ocl -lopencv_photo -lopencv_stitching -lopencv_superres -lopencv_ts -lopencv_video -lopencv_videostab

INC_DEBUG = $(INC) -Iinclude
CFLAGS_DEBUG = $(CFLAGS) -std=c++11 -pg -g -O0 -lopencv_calib3d -lopencv_contrib -lopencv_core -lopencv_features2d -lopencv_flann -lopencv_gpu -lopencv_highgui -lopencv_imgproc -lopencv_legacy -lopencv_ml -lopencv_objdetect -lopencv_ocl -lopencv_photo -lopencv_stitching -lopencv_superres -lopencv_ts -lopencv_video -lopencv_videostab
RESINC_DEBUG = $(RESINC)
RCFLAGS_DEBUG = $(RCFLAGS)
LIBDIR_DEBUG = $(LIBDIR)
LIB_DEBUG = $(LIB)
LDFLAGS_DEBUG = $(LDFLAGS) -pg -lopencv_calib3d -lopencv_contrib -lopencv_core -lopencv_features2d -lopencv_flann -lopencv_gpu -lopencv_highgui -lopencv_imgproc -lopencv_legacy -lopencv_ml -lopencv_objdetect -lopencv_ocl -lopencv_photo -lopencv_stitching -lopencv_superres -lopencv_ts -lopencv_video -lopencv_videostab
OBJDIR_DEBUG = obj/Debug
DEP_DEBUG = 
OUT_DEBUG = bin/Debug/WatertoneCPP

INC_RELEASE = $(INC) -Iinclude
CFLAGS_RELEASE = $(CFLAGS) -march=corei7-avx -O3 -std=c++11
RESINC_RELEASE = $(RESINC)
RCFLAGS_RELEASE = $(RCFLAGS)
LIBDIR_RELEASE = $(LIBDIR)
LIB_RELEASE = $(LIB)
LDFLAGS_RELEASE = $(LDFLAGS) -s
OBJDIR_RELEASE = obj/Release
DEP_RELEASE = 
OUT_RELEASE = bin/Release/WatertoneCPP

OBJ_DEBUG = $(OBJDIR_DEBUG)/src/MaskManager.o $(OBJDIR_DEBUG)/src/ImageManager.o $(OBJDIR_DEBUG)/src/Halftoning.o $(OBJDIR_DEBUG)/src/FBIH.o $(OBJDIR_DEBUG)/src/Encoder.o $(OBJDIR_DEBUG)/src/Decoder.o $(OBJDIR_DEBUG)/src/AbstractCodec.o $(OBJDIR_DEBUG)/main.o

OBJ_RELEASE = $(OBJDIR_RELEASE)/src/MaskManager.o $(OBJDIR_RELEASE)/src/ImageManager.o $(OBJDIR_RELEASE)/src/Halftoning.o $(OBJDIR_RELEASE)/src/FBIH.o $(OBJDIR_RELEASE)/src/Encoder.o $(OBJDIR_RELEASE)/src/Decoder.o $(OBJDIR_RELEASE)/src/AbstractCodec.o $(OBJDIR_RELEASE)/main.o

all: debug release

clean: clean_debug clean_release

before_debug: 
	test -d bin/Debug || mkdir -p bin/Debug
	test -d $(OBJDIR_DEBUG)/src || mkdir -p $(OBJDIR_DEBUG)/src
	test -d $(OBJDIR_DEBUG) || mkdir -p $(OBJDIR_DEBUG)

after_debug: 

debug: before_debug out_debug after_debug

out_debug: before_debug $(OBJ_DEBUG) $(DEP_DEBUG)
	$(LD) $(LIBDIR_DEBUG) -o $(OUT_DEBUG) $(OBJ_DEBUG)  $(LDFLAGS_DEBUG) $(LIB_DEBUG)

$(OBJDIR_DEBUG)/src/MaskManager.o: src/MaskManager.cpp
	$(CXX) $(CFLAGS_DEBUG) $(INC_DEBUG) -c src/MaskManager.cpp -o $(OBJDIR_DEBUG)/src/MaskManager.o

$(OBJDIR_DEBUG)/src/ImageManager.o: src/ImageManager.cpp
	$(CXX) $(CFLAGS_DEBUG) $(INC_DEBUG) -c src/ImageManager.cpp -o $(OBJDIR_DEBUG)/src/ImageManager.o

$(OBJDIR_DEBUG)/src/Halftoning.o: src/Halftoning.cpp
	$(CXX) $(CFLAGS_DEBUG) $(INC_DEBUG) -c src/Halftoning.cpp -o $(OBJDIR_DEBUG)/src/Halftoning.o

$(OBJDIR_DEBUG)/src/FBIH.o: src/FBIH.cpp
	$(CXX) $(CFLAGS_DEBUG) $(INC_DEBUG) -c src/FBIH.cpp -o $(OBJDIR_DEBUG)/src/FBIH.o

$(OBJDIR_DEBUG)/src/Encoder.o: src/Encoder.cpp
	$(CXX) $(CFLAGS_DEBUG) $(INC_DEBUG) -c src/Encoder.cpp -o $(OBJDIR_DEBUG)/src/Encoder.o

$(OBJDIR_DEBUG)/src/Decoder.o: src/Decoder.cpp
	$(CXX) $(CFLAGS_DEBUG) $(INC_DEBUG) -c src/Decoder.cpp -o $(OBJDIR_DEBUG)/src/Decoder.o

$(OBJDIR_DEBUG)/src/AbstractCodec.o: src/AbstractCodec.cpp
	$(CXX) $(CFLAGS_DEBUG) $(INC_DEBUG) -c src/AbstractCodec.cpp -o $(OBJDIR_DEBUG)/src/AbstractCodec.o

$(OBJDIR_DEBUG)/main.o: main.cpp
	$(CXX) $(CFLAGS_DEBUG) $(INC_DEBUG) -c main.cpp -o $(OBJDIR_DEBUG)/main.o

clean_debug: 
	rm -f $(OBJ_DEBUG) $(OUT_DEBUG)
	rm -rf bin/Debug
	rm -rf $(OBJDIR_DEBUG)/src
	rm -rf $(OBJDIR_DEBUG)

before_release: 
	test -d bin/Release || mkdir -p bin/Release
	test -d $(OBJDIR_RELEASE)/src || mkdir -p $(OBJDIR_RELEASE)/src
	test -d $(OBJDIR_RELEASE) || mkdir -p $(OBJDIR_RELEASE)

after_release: 

release: before_release out_release after_release

out_release: before_release $(OBJ_RELEASE) $(DEP_RELEASE)
	$(LD) $(LIBDIR_RELEASE) -o $(OUT_RELEASE) $(OBJ_RELEASE)  $(LDFLAGS_RELEASE) $(LIB_RELEASE)

$(OBJDIR_RELEASE)/src/MaskManager.o: src/MaskManager.cpp
	$(CXX) $(CFLAGS_RELEASE) $(INC_RELEASE) -c src/MaskManager.cpp -o $(OBJDIR_RELEASE)/src/MaskManager.o

$(OBJDIR_RELEASE)/src/ImageManager.o: src/ImageManager.cpp
	$(CXX) $(CFLAGS_RELEASE) $(INC_RELEASE) -c src/ImageManager.cpp -o $(OBJDIR_RELEASE)/src/ImageManager.o

$(OBJDIR_RELEASE)/src/Halftoning.o: src/Halftoning.cpp
	$(CXX) $(CFLAGS_RELEASE) $(INC_RELEASE) -c src/Halftoning.cpp -o $(OBJDIR_RELEASE)/src/Halftoning.o

$(OBJDIR_RELEASE)/src/FBIH.o: src/FBIH.cpp
	$(CXX) $(CFLAGS_RELEASE) $(INC_RELEASE) -c src/FBIH.cpp -o $(OBJDIR_RELEASE)/src/FBIH.o

$(OBJDIR_RELEASE)/src/Encoder.o: src/Encoder.cpp
	$(CXX) $(CFLAGS_RELEASE) $(INC_RELEASE) -c src/Encoder.cpp -o $(OBJDIR_RELEASE)/src/Encoder.o

$(OBJDIR_RELEASE)/src/Decoder.o: src/Decoder.cpp
	$(CXX) $(CFLAGS_RELEASE) $(INC_RELEASE) -c src/Decoder.cpp -o $(OBJDIR_RELEASE)/src/Decoder.o

$(OBJDIR_RELEASE)/src/AbstractCodec.o: src/AbstractCodec.cpp
	$(CXX) $(CFLAGS_RELEASE) $(INC_RELEASE) -c src/AbstractCodec.cpp -o $(OBJDIR_RELEASE)/src/AbstractCodec.o

$(OBJDIR_RELEASE)/main.o: main.cpp
	$(CXX) $(CFLAGS_RELEASE) $(INC_RELEASE) -c main.cpp -o $(OBJDIR_RELEASE)/main.o

clean_release: 
	rm -f $(OBJ_RELEASE) $(OUT_RELEASE)
	rm -rf bin/Release
	rm -rf $(OBJDIR_RELEASE)/src
	rm -rf $(OBJDIR_RELEASE)

.PHONY: before_debug after_debug clean_debug before_release after_release clean_release

