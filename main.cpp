#include <iostream>
#include <vector>
#include <tuple>
#include <opencv2/opencv.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>

#include <chrono>

#include "include/MaskManager.h"
#include "include/ImageManager.h"
#include "include/Halftoning.h"
#include "include/Encoder.h"
#include "include/Decoder.h"
#include "include/FBIH.h"

using namespace std::chrono;

int main()
{

//    MaskManager mm;
//    vector<mask_t> masks = mm.getMasks();
//    vector<triplet_t> triplets = mm.getTriplets();
//    std::cout << "Hello world! " << masks.size() << " " << triplets.size() << std::endl;
//    std::cout << "is 64? " << (mm.getMasks(triplets[0])).size() << std::endl;

    cv::Mat image = cv::imread("images/Cupdake.jpg");
//    ImageManager im(image);
//    vector<bgr_pixel> vectorzao = im.getBgrVector();
//    for (auto i : vectorzao) {
//        std::cout << i.B << std::endl;
//    }

    high_resolution_clock::time_point t1 = high_resolution_clock::now();
    Encoder enc(image, image);
    high_resolution_clock::time_point t2 = high_resolution_clock::now();
    auto duration_encoder = std::chrono::duration_cast<std::chrono::nanoseconds>( t2 - t1 ).count();
    cv::imwrite( "coded/Cupdake_coded.png", enc.getMasked());
    //cv::imwrite( "coded/Cupdake_host.png", enc.getHostHalftone());

    high_resolution_clock::time_point t3 = high_resolution_clock::now();
    Decoder dec(enc.getMasked());
    high_resolution_clock::time_point t4 = high_resolution_clock::now();
    auto duration_decoder = std::chrono::duration_cast<std::chrono::nanoseconds>(t4- t3).count();
    cv::imwrite( "decoded/Cupdake_decoded_invhlt.png", dec.getDecoded());

    std::cout << "Encoding:" << duration_encoder / 1e9 << " Decoding: " << duration_decoder / 1e9 << std::endl;

   // cv::Mat half = Halftoning::floydSteinberg(image);
   // cv::Mat inv = FBIH::inverseHalftoning(half);
   // cv::imwrite( "decoded/Cupdake_half.png", half);
   // cv::imwrite( "decoded/Cupdake_host_invht.png", inv);
    //std::cout << inv << std::endl;

    //cv::Mat half = Halftoning::floydSteinberg(image);
    //cv::namedWindow( "Display window", cv::WINDOW_AUTOSIZE);
    //cv::imshow("Display window", enc.getMasked());
    //cv::waitKey(0);
    return 0;
}
