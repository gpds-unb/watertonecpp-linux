#include "Decoder.h"

#include "FBIH.h"

Decoder::Decoder(cv::Mat c) {
    coded = c;
    computeHalftoneMasks(c);
    computeTripletsFromMasks();
    recoverHalftoneChannels();
    recoverColorChannels();
}

Decoder::~Decoder() {
    //dtor
}

cv::Mat Decoder::getDecoded(void) {
    return decoded;
}

// TODO: Use invhlt here
void Decoder::recoverColorChannels(void) {
    R = FBIH::inverseHalftoning(hR);
    G = FBIH::inverseHalftoning(hG);
    B = FBIH::inverseHalftoning(hB);

//    cv::namedWindow("Display window", cv::WINDOW_AUTOSIZE );
//    cv::imshow( "Display window", B);
//    cv::waitKey(0);

    vector<cv::Mat> channels = {B, G, R};
    cv::merge(channels, decoded);
}

void Decoder::recoverHalftoneChannels(void) {
    uint width = coded.cols / BLOCK_SIZE;
    uint height = coded.rows / BLOCK_SIZE;
    hR = cv::Mat::zeros(height, width, CV_8UC1);
    hG = cv::Mat::zeros(height, width, CV_8UC1);
    hB = cv::Mat::zeros(height, width, CV_8UC1);
    uint k = 0;
    for (uint i = 0; i < height; i++) {
        for (uint j = 0; j < width; j++) {
            triplet_t triplet = triplets[k++];
            hR.at<uchar>(i, j) = (uchar) triplet.t0;
            hG.at<uchar>(i, j) = (uchar) triplet.t1;
            hB.at<uchar>(i, j) = (uchar) triplet.t2;
        }
    }
}

void Decoder::computeTripletsFromMasks(void) {
    for (mask_t mask : maskedHalftoneBlocks) {
        triplet_t triplet = mm.getTripletFromMask(mask);
        triplets.push_back(triplet);
    }
}
