#include <iostream>
#include <random>
#include <limits>
#include "Encoder.h"

Encoder::Encoder(cv::Mat host, cv::Mat mark) {
    setRGB(mark);
    computeHostHalftone(host);
    computeMarkHalftones();
    computeHalftoneMasks(hostHalftone);
    computeTriplets();
    embedMark();
    convertEmbeddedMasksToHalftone();
    //std::cout << codedHalftone << std::endl;
}

Encoder::~Encoder() {
    //dtor
}

cv::Mat Encoder::getHostHalftone(void) {
    return 255 * hostHalftone;
}

cv::Mat Encoder::getMasked(void) {
    return 255 * codedHalftone;
}

void Encoder::convertEmbeddedMasksToHalftone(void) {
    uint cols = hR.cols;
    uint rows = hR.rows;
    codedHalftone = cv::Mat::zeros(BLOCK_SIZE * rows, BLOCK_SIZE * cols, CV_8UC1);
    uint k = 0;
    for(uint row=0; row < rows; row++) {
        for(uint col=0; col < cols; col++) {
            mask_t m = embeddedMasks[k++];
            cv::Mat block = getMaskAsBlock(m);
            cv::Rect roi = cv::Rect(col * BLOCK_SIZE, row * BLOCK_SIZE, BLOCK_SIZE, BLOCK_SIZE);
            //std::cout << row << "," << col << " -> " << roi << std::endl;
            block.copyTo(codedHalftone(roi));
        }
    }
}

void Encoder::embedMark(void) {
    embeddedMasks.resize(triplets.size());
    for(uint v = 0; v < triplets.size(); v++) {
        triplet_t t = triplets[v];
        mask_t b = maskedHalftoneBlocks[v];
        embeddedMasks[v] = getBestMatch(t, b);
    }
}

mask_t Encoder::getBestMatch(triplet_t triplet, mask_t mask) {
    vector<mask_t> possibleMasks = mm.getMasks(triplet);
    vector<float> distances = computeDistances(mask, possibleMasks);
    uint minDistancePosition = getMinimumIndex(distances);
    mask_t matchedMask = possibleMasks[minDistancePosition];
    //std::cout << minDistancePosition << std::endl;
    return matchedMask;
}

uint Encoder::getMinimumIndex(vector<float> distances) {
    uint minIndex = 0;
    float minimal = std::numeric_limits<float>::max();
    for (uint i = 0; i < distances.size(); i++) {
        float d = distances[i];
        if (d < minimal) {
            minimal = d;
            minIndex = i;
        }
    }

    float minIndex2 = std::min_element(distances.begin(), distances.end()) - distances.begin();
    //std::cout << minIndex << ", " << minIndex2 << ", " << (minIndex == minIndex2) << std::endl;
    return minIndex;
}

vector<float> Encoder::computeDistances(mask_t mask, vector<mask_t> possibleMasks) {
    vector<float> distances(possibleMasks.size());
    for(uint v=0; v < possibleMasks.size(); v++) {
        mask_t possibleMask = possibleMasks[v];
        distances[v] = getSimmilarityIndex(mask, possibleMask);
    }
    return distances;
}

float Encoder::getSimmilarityIndex(mask_t u, mask_t v) {
    int nft = (!u.m0 & v.m0);
    nft += (!u.m1 & v.m1);
    nft += (!u.m2 & v.m2);
    nft += (!u.m3 & v.m3);
    nft += (!u.m4 & v.m4);
    nft += (!u.m5 & v.m5);
    nft += (!u.m5 & v.m6);
    nft += (!u.m7 & v.m7);
    nft += (!u.m8 & v.m8);
    int ntf = (u.m0 & !v.m0);
    ntf += (u.m1 & !v.m1);
    ntf += (u.m2 & !v.m2);
    ntf += (u.m3 & !v.m3);
    ntf += (u.m4 & !v.m4);
    ntf += (u.m5 & !v.m5);
    ntf += (u.m5 & !v.m6);
    ntf += (u.m7 & !v.m7);
    ntf += (u.m8 & !v.m8);
    int ntt = (u.m0 & v.m0);
    ntt += (u.m1 & v.m1);
    ntt += (u.m2 & v.m2);
    ntt += (u.m3 & v.m3);
    ntt += (u.m4 & v.m4);
    ntt += (u.m5 & v.m5);
    ntt += (u.m5 & v.m6);
    ntt += (u.m7 & v.m7);
    ntt += (u.m8 & v.m8);
    float dissimilarity = (float)(ntf + nft) / (2.0 * ntt + ntf + nft);
    return dissimilarity;
}

void Encoder::computeTriplets(void) {
    cv::Mat img = this->hR;
    for(int r = 0; r < img.rows; r++) {
        for(int c = 0; c < img.cols; c++) {
            bool rr = hR.at<uchar>(r, c) != 0;
            bool gg = hG.at<uchar>(r, c) != 0;
            bool bb = hB.at<uchar>(r, c) != 0;
            triplet_t m = {
                rr, gg, bb
            };
            triplets.push_back(m);
        }
    }
}

void Encoder::computeHostHalftone(cv::Mat host) {
    cv::Mat resized;
    cv::resize(host, resized, cv::Size(0,0), BLOCK_SIZE, BLOCK_SIZE, cv::INTER_CUBIC);
    this->hostHalftone = Halftoning::floydSteinberg(resized);
}

void Encoder::computeMarkHalftones(void) {
    this->hB = Halftoning::floydSteinberg(this->B);
    this->hG = Halftoning::floydSteinberg(this->G);
    this->hR = Halftoning::floydSteinberg(this->R);
}

void Encoder::setRGB(cv::Mat mark) {
    vector<cv::Mat> rgb;
    split(mark, rgb);
    this->B = rgb[0];
    this->G = rgb[1];
    this->R = rgb[2];
}
